INTRODUCTION
------------
The Node System Links module provides a simple input filter which replaces
the output of internal node aliased links with the system links in any field
for which the text input format is enabled. The aliased node link will become
<a href="/node/99">.

It works with internal nodes links and can easily be extended to include
links to taxonomies and users.

It supports the output from the common WYSIWYG text editors including CKEditor
and TinyMCE.

INSTALLING
----------
See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

Once the Node System Links module is installed and enabled, you can go to your
"Text formats" page (/admin/config/content/formats) to configure any text
format (filtered HTML/Full HTML, etc) to use the included filter.

FREQUENTLY ASKED QUESTIONS (FAQ)
--------------------------------
There are no frequently asked questions at this time. Feel free to change
that if you have any questions.

MORE INFORMATION
----------------
To issue any bug reports, feature or support requests, see the module's issue
queue at http://drupal.org/project/issues/nodesyslinks.


HOW CAN YOU CONTRIBUTE?
-----------------------
- Report any bugs, feature requests, etc. in the issue tracker, and/or write
  patches if your coding skills are up to it and you have the time.
  http://drupal.org/project/issues/nodesyslinks

- Help translate this module.
  http://localize.drupal.org/translate/projects/nodesyslinks

- Write a review for this module at drupalmodules.com.
  http://drupalmodules.com/module/nodesyslinks
