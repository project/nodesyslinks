<?php

namespace NodeSysLinks; {

  /**
   * SysLinksFilte class.
   */
  class SysLinksFilter {

    /**
     * Process regex matches of links for nodesyslinks_title_filter_process().
     *
     * @param array $matches
     *   Parameter passed by nodesyslinks_title_filter_process();
     *   an array from a regex match to an HTML link.
     * @param string $langcode
     *   Language of the current node.
     *
     * @return mixed
     *   Original link, possibly modified to include the node system path.
     */
    public function processLink(array $matches, $langcode) {
      static $language_list = array();

      if (empty($language_list)) {
        $language_list = language_list();
      }

      $parts = parse_url($matches[2]);

      // Skip absolute links in the content which can be external.
      if (!array_key_exists('host', $parts) && !empty($parts['path'])) {
        // @todo: Paths with absolute links.
        $parts['path'] = trim($parts['path'], '/');
        // Skip any internal system links.
        if (!preg_match('@node/\d+$@', $parts['path'])) {
          // Get last part of the URL.
          $parts = array_values($parts);
          $possible_alias = end($parts);
          // Get the system path.
          $sys_path = drupal_lookup_path('source', $possible_alias, NULL);

          // Replace node aliased path with system path.
          // @todo: Extend functionality to include taxonomies and users.
          if ($this->setPath($matches, $sys_path)) {
            $matches[0] = $this->setPath($matches, $sys_path);
          }
        }
      }

      return $matches[0];
    }

    /**
     * Modifies the node path when the text input filter is used.
     */
    protected function setPath($matches, $sys_path) {
      $sys_path_parts = explode('/', $sys_path);

      // Replaces node aliased path with system path.
      if (!empty($sys_path_parts[0]) && $sys_path_parts[0] === 'node') {
        return str_replace($matches[2], '/' . $sys_path, $matches[0]);
      }
    }

    /**
     * Returns the declared properties for hook_filter_info().
     */
    public function filterInfo() {
      $filters['nodesyslinks link'] = array(
        'title' => t('Convert aliases to system links'),
        'description' => t('Identifies node aliased links in content and
          replaces them with system links (e.g. node/123).'),
        'process callback' => 'nodesyslinks_link_filter_process',
        'default settings' => array('nodesyslinks_link_display_tip' => 1),
        'tips callback' => 'nodesyslinks_link_filter_tips',
      );

      return $filters;
    }

    /**
     * Returns the help text for use in hook_help().
     */
    public function helpText($path) {
      switch ($path) {
        case 'admin/help#nodesyslinks':
          $output = '<p>' . t('To use the Node  system links filter, simply
            enable the Node System Links for one or more of your
            <a href="@text_formats">text formats</a>.', array(
              '@text_formats' => url('admin/config/content/formats'),
            )) . '</p><p>';
          $output .= t('For best performance, sites which uses this filter
          should cache the output. Use of this filter on each page load will add
          significant additional load in terms of database queries and
          processing.') . '</p><p>';
          $output .= t('Caching output is especially recommended for high-volume
            sites with a lot of cross-linking between articles.') . '</p>';

          return $output;
      }
    }

  }
}
